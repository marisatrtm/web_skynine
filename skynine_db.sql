-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 06, 2018 at 12:52 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skynine_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_admin`
--

CREATE TABLE `account_admin` (
  `username` varchar(150) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account_admin`
--

INSERT INTO `account_admin` (`username`, `password`) VALUES
('marketing01', 'dcb7d8e2e27c4a9b8c58751d73068fd9'),
('finance01', 'edb8c523f11478f6f55a49e7f5990f23');

-- --------------------------------------------------------

--
-- Table structure for table `account_user`
--

CREATE TABLE `account_user` (
  `email` varchar(250) NOT NULL,
  `password` varchar(20) NOT NULL,
  `name` varchar(150) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `address` varchar(250) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `birth_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account_user`
--

INSERT INTO `account_user` (`email`, `password`, `name`, `phone`, `address`, `gender`, `birth_date`) VALUES
('marisa.trut22@gmail.com', '123456', 'marisa', '08128056381', 'aaaaaaaaaaaaa', 'F', '2002-03-20');

-- --------------------------------------------------------

--
-- Table structure for table `artikels`
--

CREATE TABLE `artikels` (
  `artikel_id` int(11) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `author` varchar(80) NOT NULL,
  `konten` text NOT NULL,
  `kategori` varchar(250) NOT NULL,
  `added_date` date NOT NULL,
  `publish` varchar(50) NOT NULL,
  `modified_date` date NOT NULL,
  `Image` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `products_id` varchar(150) NOT NULL,
  `name` varchar(250) NOT NULL,
  `details` text NOT NULL,
  `price` varchar(50) NOT NULL,
  `tipe` varchar(250) NOT NULL,
  `images` varchar(500) NOT NULL,
  `addedby` varchar(500) NOT NULL,
  `upload_date` date NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`products_id`, `name`, `details`, `price`, `tipe`, `images`, `addedby`, `upload_date`, `quantity`) VALUES
('bali_honey', 'Bali Honey', 'Flavour Notes : Black Tea, hazelnut, honey like.\r\nMdpl : 1.300\r\n\r\nRecommendation\r\nGrind Size : Medium\r\nTemp : 91 derajat\r\nDose : 15 gr\r\nYield : 225 ml\r\nBrew Time : 2.00 min', '', '', '', '', '0000-00-00', 0),
('product_id', 'name', 'details', 'price', 'tipe', 'images', 'addedby', '0000-00-00', 0),
('C001', 'Bali Honey ', 'Flavour Notes 1kg', 'Rp240.000', 'hazelnut, Black tea, honey like', '', 'SkyNine', '0000-00-00', 10),
('C002', 'Bali Natural ', 'Flavour Notes 1kg', 'Rp240.000', 'Honey like, Chocolate, pineapple', '', 'SkyNine', '0000-00-00', 10),
('C003', 'Balok Sari ', 'Flavour Notes, Stronger Coffee taste 1kg', '', 'Chocolate, nutty. Blend Component', '', 'SkyNine', '0000-00-00', 10),
('C004', 'Gayo ', 'Flavour Notes 1kg', 'Rp240.000', 'Black tea, tamarind, floral', '', 'SkyNine', '0000-00-00', 10),
('C005', 'Kerinci ', 'Flavour Notes 1kg', 'Rp240.000', 'Caramel, green apple, grapes', '', 'SkyNine', '0000-00-00', 10),
('C006', 'Kopi Susu ', 'Flavour Notes, Strong Flavour 1kg', 'Rp80.000', 'Hazelnut, chocolate, peanut butter', '', 'SkyNine', '0000-00-00', 10),
('C007', 'Luna ', 'Flavour Notes, Bold and Full bodied 1kg', '', 'Chocolate, huzelnut, nutty, cinnamon', '', 'SkyNine', '0000-00-00', 10),
('C008', 'Pucak Sari ', 'Flavour Notes 1kg', 'Rp170.000', 'Dark Chocolate, nutty, oak', '', 'SkyNine', '0000-00-00', 10),
('C009', 'Smooth Operator', 'Flavour Notes 1kg', 'Rp250.000', 'Orange mouthfeel, brown sugar, spicy', '', 'SkyNine', '0000-00-00', 10),
('C010', 'Solok', 'Flavour Notes 1kg', 'Rp240.000', 'Chocolate. Blend Component', '', 'SkyNine', '0000-00-00', 10),
('C011', 'Toraja ', 'Flavour Notes 1kg', 'Rp240.000', 'Chocolate, yellow watermelon, honey like', '', 'SkyNine', '0000-00-00', 10),
('C012', 'Kintamani ', 'Flavour Notes 50kg', 'Rp6.000.000', '', '', 'SkyNine', '0000-00-00', 10),
('C013', 'Bali Honey ', 'Flavour Notes 250gr', 'Rp75.000', 'hazelnut, Black tea, honey like', '', 'SkyNine', '0000-00-00', 10),
('C014', 'Bali Natural ', 'Flavour Notes 250gr', 'Rp75.000', 'Honey like, Chocolate, pineapple', '', 'SkyNine', '0000-00-00', 10),
('C015', 'Balok Sari ', 'Flavour Notes, Stronger Coffee taste 250gr', '', 'Chocolate, nutty. Blend Component', '', 'SkyNine', '0000-00-00', 10),
('C016', 'Colombia ', 'Flavour Notes 250gr', 'Rp90.000', 'Honey like, chocolate, orange like', '', 'SkyNine', '0000-00-00', 10),
('C017', 'Ethiopia Sidamo ', 'Flavour Notes 250gr', '', 'Ginger, black tea, palm sugar, chocolate', '', 'SkyNine', '0000-00-00', 10),
('C018', 'Gayo ', 'Flavour Notes 250gr', 'Rp75.000', 'Black tea, tamarind, floral', '', 'SkyNine', '0000-00-00', 10),
('C019', 'Kenya', 'Flavour Notes 250gr', 'Rp90.000', '', '', 'SkyNine', '0000-00-00', 10),
('C020', 'Kerinci ', 'Flavour Notes 250gr', 'Rp75.000', 'Green apple, caramel, grapes', '', 'SkyNine', '0000-00-00', 10),
('C021', 'Kopi Susu ', 'Flavour Notes 250gr', 'Rp20.000 ', 'Hazelnut, chocolate, peanut butter', '', 'SkyNine', '0000-00-00', 10),
('C022', 'Luna ', 'Flavour Notes, Bold and Full bodied 250gr', '', 'Chocolate, huzelnut, nutty, cinnamon', '', 'SkyNine', '0000-00-00', 10),
('C023', 'Pucak Sari ', 'Flavour Notes 250gr', 'Rp60.000', 'Dark Chocolate, nutty, oak', '', 'SkyNine', '0000-00-00', 10),
('C024', 'Smooth Operator ', 'Flavour Notes, Smoothly Design for daily Coffee intake 250gr', 'Rp90.000', 'Orange mouthfeel, brown sugar, spicy', '', 'SkyNine', '0000-00-00', 10),
('C025', 'Solok', 'Flavour Notes 250gr', 'Rp75.000', 'Orange mouthfeel, brown sugar, spicy', '', 'SkyNine', '0000-00-00', 10),
('C026', 'Toraja ', 'Flavour Notes 250gr', '', 'Chocolate, yellow watermelon, honey like', '', 'SkyNine', '0000-00-00', 10);

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `transaction_id` int(11) NOT NULL,
  `email` varchar(250) NOT NULL,
  `date` date NOT NULL,
  `total_price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trans_detail`
--

CREATE TABLE `trans_detail` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trans_payment`
--

CREATE TABLE `trans_payment` (
  `id` int(11) NOT NULL,
  `address` varchar(250) NOT NULL,
  `no_rekening` varchar(50) NOT NULL,
  `nama_pemilik_rek` varchar(150) NOT NULL,
  `bukti_trf` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `workshop`
--

CREATE TABLE `workshop` (
  `workshop_id` int(11) NOT NULL,
  `judul_workshop` varchar(250) NOT NULL,
  `deskripsi` text NOT NULL,
  `hari` varchar(10) NOT NULL,
  `tanggal` date NOT NULL,
  `alamat` varchar(250) NOT NULL,
  `jam_pelaksanaan` time NOT NULL,
  `video_url` varchar(500) NOT NULL,
  `added by` varchar(50) NOT NULL,
  `modified date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
