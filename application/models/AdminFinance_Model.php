<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//nama class harus sesuai dengan nama .php di controller
class AdminFinance_Model extends CI_Model{
	public function __construct(){
		parent::__construct();
		$this->load->database();
		//$this->load->model('AdminFinance_Model');
	}

	// read member
	public function getAllMember() {
		$db=$this->load->database('default', true);
		$sql="SELECT * from account_user";
		$qry=$db->query($sql);
		$query=$qry->result_array();
		return $query;
	}

	// read transaction
	public function getAllTransaction() {
		$db=$this->load->database('default', true);
		$sql="SELECT t.transaction_id, au.user_name, p.product_name, td.price, td.quantity, tp.address, t.email, t.date, t.total_price, tp.no_rekening, td.product_id, t.status from transactions t
			INNER JOIN trans_detail td
			on td.id=t.transaction_id
			INNER JOIN account_user au
			on au.email = t.email
			INNER JOIN products p
			on p.products_id=td.product_id
			INNER JOIN trans_payment tp 
			on tp.id=t.transaction_id
			";
		$qry=$db->query($sql);
		$query=$qry->result_array();
		return $query;
	}

	// update memberResi
	public function updateResi() {
		$db=$this->load->database('default', true);
		$sql="UPDATE transactions SET ";
		$qry=$db->query($sql);
		$query=$qry->result_array();
		return $query;
		/*$query = $this->db->get_where('', array('email' => $email));
        return $query->row();*/
	}

	public function logout()  
    {  
    	$this->session->unset_userdata('email');  
       	redirect(base_url() . 'index.php/NoLogin_HomeView_Controller/index');  
    } 
}
?>