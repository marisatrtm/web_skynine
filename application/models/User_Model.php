<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    class User_model extends CI_model{
      public $table='account_user';
      
      public function login($email, $password){
        echo"<script>console.log('email: ".$email." dan password: ".$password."');</script>";
        $query = $this->db->get_where('account_user', array('email' => $email, 'password'=> $password));
        return $query->row();
      }

      public function checkData($email){
        $query = $this->db->get_where('account_user', array('email' => $email));
        return $query->row();
      }

      public function getEmps(){
        $this->db->select("*");
        $this->db->from("account_user");
        $result = $this->db->get();
        return $result->result_array();
      }

      public function addEmp($name, $birth_date, $gender, $address, $phone, $email, $password){
        $data = array(
            
            'name' => $name,
            'birth_date' => $birth_date,
            'gender' => $gender,
            'address' => $address,
            'phone' => $phone,
            'email' => $email,
            'password' => $password
        );

        return $this->db->insert('account_user', $data);
      }

      public function get_user($email){
        $query = $this->db->query("SELECT * FROM account_user WHERE email = '$email'");
        return $query->result_array();
      }

      public function sanity($anything){
        $anything = htmlspecialchars($anything, ENT_QUOTES);
        $anything = strip_tags($anything);
        $anything = stripcslashes($anything);
        return $anything;
      }

      public function update($email,$data)
      {
        // $this->db->where('email', $email);
        // $this->db->update('account_user', $values);

        $query = $this->db->where('email', $email)->update($this->table, $data);

        return $query;
      }
    }
?>