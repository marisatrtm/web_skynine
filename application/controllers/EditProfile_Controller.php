<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//nama class harus sesuai dengan nama .php di controller
class EditProfile_Controller extends CI_Controller{

	public function __construct(){
		parent::__construct();

		$this->load->model('User_model');//sesuai nama yang di models
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|xss_clean');
		$this->load->helper(array('form', 'url','security'));//helper dari CI yang dipake
	}

	public function index(){
		$data['session'] = $this->session->userdata('email'); // create variable session dengan menggunakan email
		$user = $this->session->userdata('email');
		$data['user'] = $this->User_model->get_user($user);
		$this->load->view('pages/editProfile');
	}

	public function logout()  
    {  
    	$this->session->unset_userdata('email');  
       	redirect(base_url() . 'index.php/NoLogin_HomeView_Controller/index');  
    } 

    public function insert(){
		if($this->input->post('submit')){

	  //       $this->load->library('upload', $config);
			// $this->upload->initialize($config);

			// $values = array(
			// 	'name' => $this->User_model->sanity($this->input->post('name')),
			// 	// 'gender' => $this->User_model->sanity($this->input->post('gender')),
			// 	'phone' => $this->User_model->sanity($this->input->post('phone')),
			// 	'address' => $this->User_model->sanity($this->input->post('address'))
				
			// );
			$data['name'] = $this->input->post('name');
			$data['phone'] = $this->input->post('phone');
			$data['address'] = $this->input->post('address');

			$user = $this->session->userdata('email');
			$this->User_model->update($user, $data);

			redirect(base_url() . 'index.php/Home_Controller/index');
		}		
	}
}
?> 