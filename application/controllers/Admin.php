<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function index()
	{
		// $this->load->helper('url');
		$this->load->view('pages/admin');
	}
	public function logout()  
    {  
    	$this->session->unset_userdata('email');  
       	redirect(base_url() . 'index.php/NoLogin_HomeView_Controller/index');  
    } 
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */