<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//nama class harus sesuai dengan nama .php di controller
class SignUp_Controller extends CI_Controller{

	public function __construct(){
		parent::__construct();
  		$this->load->helper('url');
  		$this->load->model('User_Model');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('password','Password','required');
	}

	public function index(){
		// $data['js'] = $this->load->view('include/js', NULL, TRUE);
		// $data['css'] = $this->load->view('include/css', NULL, TRUE);
		// $this->load->view('pages/signup',$data);
		$this->load->view('pages/signup');
	}


	public function addUser(){
        $name=$this->input->post('name');
        $birth_date=$this->input->post('birth_date');
        $gender=$this->input->post('gender');
        $address=$this->input->post('address');
        $phone=$this->input->post('phone');
        $email=$this->input->post('email');
        $password=$this->input->post('password');

        $check_data = $this->User_Model->checkData($email);
        if($check_data!=null){
        	$this->load->view('pages/modal/signup_failed');
        }
        else{
        	$result = $this->User_Model->addEmp($name,$birth_date,$gender,$address,$phone,$email,$password);
        	$this->load->view('pages/modal/signup_success.php');
        }
	}

	public function modal_signup_success(){
		$this->load->view('pages/modal/signup_success.php');
	}
}
?>