<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//nama class harus sesuai dengan nama .php di controller
class NoLogin_CoffeeList_Controller extends CI_Controller{
	public function index(){
		$data['product'] = $this->Coffee_Model->get_product();
		$this->load->view('pages/nologin_coffeelist');
	}
}
?> 