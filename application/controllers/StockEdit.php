<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class StockEdit extends CI_Controller {
	// public function __construct(){

	// }

	public function index()
	{
		//$this->load->helper('url');
		$this->load->view('pages/admin_stock_edit');
	}
	public function logout()  
    {  
    	$this->session->unset_userdata('email');  
       	redirect(base_url() . 'index.php/NoLogin_HomeView_Controller/index');  
    } 
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */