<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//nama class harus sesuai dengan nama .php di controller
class Login_Controller extends CI_Controller{

	public function __construct(){
		parent::__construct();
  		$this->load->helper('url');
  		$this->load->model('User_Model');
        $this->load->library('session');
	}

	public function index(){
		// $data['js'] = $this->load->view('include/js', NULL, TRUE);
		// $data['css'] = $this->load->view('include/css', NULL, TRUE);
		// $this->load->view('pages/login',$data);
		$this->load->view('pages/login');
	}

	public function login_user(){
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|xss_clean');
		$this->form_validation->set_rules('password','Password','required');
		
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		if(isset($email) && isset($password)){
			if($email=="adminstock@gmail.com" && $password=="adminstock"){
				$this->session->set_userdata('email',$email);
				$this->load->view('pages/admin');
			}
			if($email=="adminfinance@gmail.com" && $password=="adminfinance"){
				$this->session->set_userdata('email',$email);
				$this->load->view('pages/adminFinance');
			}
			if($email=="adminworkshop@gmail.com" && $password=="adminworkshop"){
				$this->session->set_userdata('email',$email);
				$this->load->view('pages/adminWorkshop');
			}
			else{
				$data = $this->User_Model->login($email, $password);
				if($data!=null){
					$this->session->set_userdata('email', $data->email);
					$this->session->set_userdata('user_name', $data->user_name);
					$this->session->set_userdata('birth_date', $data->birth_date);
					$this->session->set_userdata('gender', $data->gender);
					$this->session->set_userdata('phone', $data->phone);
					$this->session->set_userdata('address', $data->address);
					$this->load->view('pages/homeView');
				}
				else{
					$data['error']="Username and Password invalid";
					$this->load->view('pages/modal/login_failed');
				}
			}
		}
	}
}
?>