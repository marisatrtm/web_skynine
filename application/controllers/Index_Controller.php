<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Index_Controller extends CI_Controller {

	public function index()
	{
		// $this->load->helper('url');
		$this->load->view('pages/homeView');
	} 
	public function logout()  
    {  
    	$this->session->unset_userdata('email');  
       	redirect(base_url() . 'index.php/NoLogin_HomeView_Controller/index');  
    } 
    public function gantiProfile(){
    	redirect(base_url() . 'index.php/Index_Controller');
    }
}
