<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//nama class harus sesuai dengan nama .php di controller
class Coffeelist_Controller extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('User_model');
		$this->load->model('Coffee_Model');
		$this->load->helper(array('form','url','security'));
	}
	public function index(){
		$data['session']='';
		$data['error']='';
		$data['coffee'] = $this->Coffee_Model->get_coffee();
		$this->load->view('pages/coffeelist');
	}
	public function logout()  
    {  
    	$this->session->unset_userdata('email');  
       	redirect(base_url() . 'index.php/NoLogin_HomeView_Controller/index');  
    } 
}
?>