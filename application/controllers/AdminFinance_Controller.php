<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//nama class harus sesuai dengan nama .php di controller
class AdminFinance_Controller extends CI_Controller{

	public function AdminFinance_Controller(){
		parent::__construct();
		$this->load->model('AdminFinance_Model');
	}

	public function index(){
		// $data['js'] = $this->load->view('include/js', NULL, TRUE);
		// $data['css'] = $this->load->view('include/css', NULL, TRUE);
		// $this->load->view('pages/coffeeEq',$data);
		$data['transaction']=$this->AdminFinance_Model->getAllTransaction();
		$this->load->view('pages/adminFinance', $data);
	}

	public function getAdminMember(){
		$data['member']=$this->AdminFinance_Model->getAllMember();
		$this->load->view('pages/adminMember', $data);
	}

	public function logout()  
    {  
    	$this->session->unset_userdata('email');  
       	redirect(base_url() . 'index.php/NoLogin_HomeView_Controller/index');  
    } 
}
?>