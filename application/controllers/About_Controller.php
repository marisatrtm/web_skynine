<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//nama class harus sesuai dengan nama .php di controller
class About_Controller extends CI_Controller{
	public function index(){
		// $data['js'] = $this->load->view('include/js', NULL, TRUE);
		// $data['css'] = $this->load->view('include/css', NULL, TRUE);
		// $this->load->view('pages/coffeeEq',$data);
		$this->load->view('pages/aboutus');
	}
	public function logout()  
    {  
    	$this->session->unset_userdata('email');  
       	redirect(base_url() . 'index.php/NoLogin_HomeView_Controller/index');  
    } 
}
?> 