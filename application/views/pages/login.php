<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login Skynine</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<!-- <link rel="icon" type="image/png" href="images/icons/favicon.ico"/> -->
<!--===============================================================================================-->
	<!-- <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css"> -->
<!--===============================================================================================-->
	<!-- <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css"> -->
<!--===============================================================================================-->
	<!-- <link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css"> -->
<!--===============================================================================================-->
	<!-- <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css"> -->
<!--===============================================================================================-->	
	<!-- <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css"> -->
<!--===============================================================================================-->
	<!-- <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css"> -->
<!--===============================================================================================-->
	<!-- <link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css"> -->
<!--===============================================================================================-->

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/evan/login/css/main.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/evan/login/css/util.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/evan/login/vendor/bootstrap/css/bootstrap.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/evan/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/evan/login/fonts/Linearicons-Free-v1.0.0/icon-font.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/evan/login/vendor/animate/animate.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/evan/login/vendor/css-hamburgers/hamburgers.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/evan/login/vendor/select2/select2.min.css'); ?>">
<link rel="icon" type="image/png" href="<?php echo base_url('assets/evan/login/images/icons/favicon.ico'); ?>">

<!-- <?php
	echo $css;
	echo $js;
?> -->

</head>
<body>

	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-l-50 p-r-50 p-t-77 p-b-30">
				<form class="login100-form validate-form" role="form" method="POST" action="<?php echo base_url('index.php/Login_Controller/login_user'); ?>">
					<span class="login100-form-title p-b-55">
						Login
					</span>

					<div class="wrap-input100 validate-input m-b-16">
						<input class="input100" type="text" name="email" placeholder="Email" required="true">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<span class="lnr lnr-envelope"></span>
						</span>
					</div>

					<div class="wrap-input100 validate-input m-b-16">
						<input class="input100" type="password" name="password" placeholder="Password" required="true">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<span class="lnr lnr-lock"></span>
						</span>
					</div>

					<!-- <div class="contact100-form-checkbox m-l-4">
						<input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
						<label class="label-checkbox100" for="ckb1">
							Remember me
						</label>
					</div> -->
					
					<div class="container-login100-form-btn p-t-25">
						<!-- <button class="login100-form-btn"><a href="<?php echo base_url('/index.php/Coffeelist_Controller/index'); ?>">
							Login</a>
						</button> -->

						<button class="login100-form-btn" type="submit" name="submit">Login
						</button>
					</div>
					<!-- <div class="container-login100-form-btn p-t-25">
						<button class="login100-form-btn"><a href="<?php echo base_url('/index.php/NoLogin_HomeView_Controller/index'); ?>"><h1 style="color: white; font-size: 16px">Cancel</h1></a>
						</button>
					</div> -->

					<div class="text-center w-full p-t-115">
						<span class="txt1">
							Not a member?
						</span>

						<a class="txt1 bo1 hov1" href="<?php echo base_url('/index.php/SignUp_Controller/index'); ?>">
							Sign up now							
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	

	
<!--===============================================================================================-->	
	<!-- <script src="vendor/jquery/jquery-3.2.1.min.js"></script> -->
<!--===============================================================================================-->
	<!-- <script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script> -->
<!--===============================================================================================-->
	<!-- <script src="vendor/select2/select2.min.js"></script> -->
<!--===============================================================================================-->
	<!-- <script src="js/main.js"></script> -->
	<script src="<?php echo base_url('/assets/evan/login/js/main.js'); ?>"></script>
	<script src="<?php echo base_url('/assets/evan/login/vendor/jquery/jquery-3.2.1.min.js'); ?>"></script>
	<script src="<?php echo base_url('/assets/evan/login/vendor/bootstrap/js/popper.js'); ?>"></script>
	<script src="<?php echo base_url('/assets/evan/login/vendor/bootstrap/js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('/assets/evan/login/vendor/select2/select2.min.js'); ?>"></script>

</body>
</html>