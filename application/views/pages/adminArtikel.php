<!DOCTYPE html>
<html>
<head>
<title>Skynine Coffee</title>
<link href="<?php echo base_url();?>assets/ica/css/bootstrap.css" rel="stylesheet" type="text/css"  />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

<!-- Custom Theme files -->
<!--theme-style-->
<link href="<?php echo base_url();?>assets/ica/css/style.css" rel="stylesheet" type="text/css"  />	
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Free Style Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--fonts-->
<link href='http://fonts.googleapis.com/css?family=Alegreya+Sans+SC:100,300,400,500,700,800,900,100italic,300italic,400italic,500italic,700italic,800italic,900italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
<!--//fonts-->
<script type="text/javascript" src="<?php echo base_url();?>assets/ica/js/move-top.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/ica/js/easing.js;"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>	
<!-- start menu -->
<script src="<?php echo base_url();?>assets/ica/js/simpleCart.min.js"> </script>
<link href="<?php echo base_url();?>assets/ica/css/memenu.css" rel="stylesheet" type="text/css"  />
<!-- Modal css/js -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<!-- Modal  css/js-->
<script type="text/javascript" src="<?php echo base_url();?>assets/ica/js/memenu.js"></script>
<script>$(document).ready(function(){$(".memenu").memenu();});</script>				
</head>
<body> 
	<!--top-header-->
	<div class="top-header">
	<div class="container">
		<div class="top-header-main">
			<div class="col-md-4 top-header-left">
				<!-- <div class="search-bar">
					<input type="text" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}">
					<input type="submit" value="">
				</div> -->
			</div>
			<div class="col-md-4 top-header-middle">
				<a href="<?php echo base_url()?>index.php/Home"><img src="<?php echo base_url()?>assets/images/skynine.jpg" alt="" style="width: 125px; height: 125px;" /></a>
			</div>
			<div class="col-md-4 top-header-right">
				<div class="cart box_1">
					<a href="<?php echo base_url('/index.php/Admin/logout'); ?>">LOGOUT
						<span class="glyphicon glyphicon-user"></span>
					</a>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div> 
<!--top-header-->
	<!-- header bottom -->
	<div class="header-bottom">
		<div class="container">
			<div class="top-nav">
				<h2 style="text-align: center;">Artickels</h2>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
	<!-- nav-bar -->
	<div class="header-bottom">
		<div class="container">
			<div class="top-nav">
				<ul class="memenu skyblue">
					<li class="grid"><a href="<?php echo base_url();?>index.php/Admin_Artikel_Controller">Artikel</a>
					</li>
					<li class="grid"><a href="<?php echo base_url();?>index.php/AdminWorkshop_Controller">Workshop</a></li>
				</ul>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
	<!-- close nav-bar -->

	<div>
		<a href="<?php echo base_url();?>index.php/AdminAddArtikel_Controller" class="btn btn-success">
			<i class="glyphicon glyphicon-plus"></i>
			Add New Artikel
		</a>
	</div>
	<!-- close header bottom -->

	<!-- <div>
		<a href="<?php echo base_url();?>index.php/AddStock" class="btn btn-success">
			<i class="glyphicon glyphicon-plus"></i>
			Add New Product
		</a>
	</div> -->
	
	<div>
		<!-- isinya -->
		<table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
		  <thead>
		    <tr>
		      <th class="th-sm">Artikel ID
		        <i class="fa fa-sort float-right" aria-hidden="true"></i>
		      </th>
		      <th class="th-sm">Judul Artikel
		        <i class="fa fa-sort float-right" aria-hidden="true"></i>
		      </th>
		      <th class="th-sm">Penulis
		        <i class="fa fa-sort float-right" aria-hidden="true"></i>
		      </th>
		      <th class="th-sm">Konten
		        <i class="fa fa-sort float-right" aria-hidden="true"></i>
		      </th>
		      <th class="th-sm">Kategori
		        <i class="fa fa-sort float-right" aria-hidden="true"></i>
		      </th>
		      <th class="th-sm">Image
		        <i class="fa fa-sort float-right" aria-hidden="true"></i>
		      </th>
		      <th class="th-sm">Added Date
		        <i class="fa fa-sort float-right" aria-hidden="true"></i>
		      </th>
		      <th class="th-sm">Modified By
		        <i class="fa fa-sort float-right" aria-hidden="true"></i>
		      </th>
		      <th class="th-sm">Publish
		        <i class="fa fa-sort float-right" aria-hidden="true"></i>
		      </th>
		      <th class="th-sm">Action
		        <i class="fa fa-sort float-right" aria-hidden="true"></i>
		      </th>
		    </tr>
		  </thead>
		  <tbody>
		    <tr>
		      <td>001</td>
		      <td>marisatriutami@gmail.com</td>
		      <td>2011/04/25</td>
		      <td>Rp xxxxxxxx</td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td>
		     	<button class="btn btn-primary" type="button">Publish</button>
		      </td>
		      <td>
		      	<a href="<?php echo base_url()?>index.php/AdminEditArtikel_Controller" class="btn btn-primary">Edit</a>
		      	<button class="btn btn-danger" type="button" data-toggle="modal" data-target="#myModal">Delete</button>
		      	<!-- Modal -->
				  <div class="modal fade" id="myModal" role="dialog">
				    <div class="modal-dialog">
				    
				      <!-- Modal content-->
				      <div class="modal-content">
				        <div class="modal-header">
				          <button type="button" class="close" data-dismiss="modal">&times;</button>
				          <h4 class="modal-title">Hapus Artikel</h4>
				        </div>
				        <div class="modal-body">
				          <p>Apakah Anda yakin ingin menghapus artikel ini? </p>
				        </div>
				        <div class="modal-footer">
				          <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
				          <button type="button" class="btn btn-default" data-dismiss="modal">Dismiss</button>
				        </div>
				      </div>
				      
				    </div>
				  </div>

		      </td>
		    </tr>
		  </tbody>
		  <tfoot>
		    <tr>
		     	<th>Artikel ID</th>
		     	<th>Judul Artikel</th>
		    	<th>Penulis</th>
		    	<th>Konten</th>
		    	<th>Kategori</th>
		    	<th>Image</th>
		    	<th>Added Date</th>
		    	<th>Modified By</th>
		    	<th>Publish</th>
		    	<th>Action</th>
		    </tr>
		  </tfoot>
		</table>
	</div>
	<!--end-abt-shoe-->
	<!--start-footer-->
	<div class="footer">
		<div class="container">
			<div class="footer-top">
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!--end-footer-->
	<!--end-footer-text-->
	<div class="footer-text">
		<script type="text/javascript">
			$(document).ready(function() {
				/*
				var defaults = {
		  			containerID: 'toTop', // fading element id
					containerHoverID: 'toTopHover', // fading element hover id
					scrollSpeed: 1200,
					easingType: 'linear' 
		 		};
				*/
				
				$().UItoTop({ easingType: 'easeOutQuart' });
				
			});
		</script>
		<a href="#home" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
	</div>
	<!--end-footer-text-->	
</body>
</html>
<script type="text/javascript">
	$(document).ready(function () {
	  $('#dtMaterialDesignExample').DataTable();
	  $('#dtMaterialDesignExample_wrapper').find('label').each(function () {
	    $(this).parent().append($(this).children());
	  });
	  $('#dtMaterialDesignExample_wrapper .dataTables_filter').find('input').each(function () {
	    $('input').attr("placeholder", "Search");
	    $('input').removeClass('form-control-sm');
	  });
	  $('#dtMaterialDesignExample_wrapper .dataTables_length').addClass('d-flex flex-row');
	  $('#dtMaterialDesignExample_wrapper .dataTables_filter').addClass('md-form');
	  $('#dtMaterialDesignExample_wrapper select').removeClass('custom-select custom-select-sm form-control form-control-sm');
	  $('#dtMaterialDesignExample_wrapper select').addClass('mdb-select');
	  $('#dtMaterialDesignExample_wrapper .mdb-select').material_select();
	  $('#dtMaterialDesignExample_wrapper .dataTables_filter').find('label').remove();
	});
</script>
