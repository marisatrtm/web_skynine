<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Colorlib Templates">
    <meta name="author" content="Colorlib">
    <meta name="keywords" content="Colorlib Templates">

    <!-- Title Page-->
    <title>Sign Up Skynine</title>

    <!-- Icons font CSS-->
    <!-- <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all"> -->
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Vendor CSS-->
    <!-- <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all"> -->

    <!-- Main CSS-->
    <!-- <link href="css/main.css" rel="stylesheet" media="all"> -->

    <link rel="stylesheet" media="all" href="<?php echo base_url('assets/evan/signup/vendor/mdi-font/css/material-design-iconic-font.min.css'); ?>">
    <link rel="stylesheet" media="all" href="<?php echo base_url('assets/evan/signup/vendor/font-awesome-4.7/css/font-awesome.min.css'); ?>">
    <link rel="stylesheet" media="all" href="<?php echo base_url('assets/evan/signup/vendor/select2/select2.min.css'); ?>">
    <link rel="stylesheet" media="all" href="<?php echo base_url('assets/evan/signup/vendor/datepicker/daterangepicker.css'); ?>">
    <link rel="stylesheet" media="all" href="<?php echo base_url('assets/evan/signup/css/main.css'); ?>">
<!-- <?php
    echo $css;
    echo $js;
?> -->
</head>

<body>
    <div class="page-wrapper bg-gra-02 p-t-130 p-b-100 font-poppins">
        <div class="wrapper wrapper--w680">
            <div class="card card-4">
                <div class="card-body">
                    <h2 class="title">Registration Form</h2>
                    
                    <!-- <?php
                        $error_msg=$this->session->flashdata('error_msg');
                        if($error_msg){
                            echo $error_msg;
                        }
                    ?> -->

                    <form role="form" method="POST" action="<?php echo base_url('index.php/SignUp_Controller/addUser'); ?>">
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Full name</label>
                                    <input class="input--style-4" type="text" name="name" required="true">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Birthday</label>
                                    <div class="input-group-icon">
                                        <input class="input--style-4 js-datepicker" type="date" name="birth_date" required="true">
                                        <!-- <i class="zmdi zmdi-calendar-note input-icon js-btn-calendar"></i> -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Gender</label>
                                    <div class="p-t-10">
                                        <label class="radio-container m-r-45">Male
                                            <input type="radio" checked="checked" name="gender" value="M" required="true">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="radio-container">Female
                                            <input type="radio" name="gender" value="F" required="true">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Address</label>
                                    <input class="input--style-4" type="text" name="address" required="true">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Phone Number</label>
                                    <input class="input--style-4" type="text" name="phone" required="true">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Email</label>
                                    <input class="input--style-4" type="text" name="email" required="true">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Password</label>
                                    <input class="input--style-4" type="password" name="password" required="true">
                                </div>
                            </div>
                        </div>
                        <div class="p-t-15">
                            <button class="btn btn--radius-2 btn--blue" type="submit" value="Add" name="submit">Submit</button>
                        </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Jquery JS-->
    <!-- <script src="vendor/jquery/jquery.min.js"></script> -->
    <!-- Vendor JS-->
    <!-- <script src="vendor/select2/select2.min.js"></script>
    <script src="vendor/datepicker/moment.min.js"></script>
    <script src="vendor/datepicker/daterangepicker.js"></script> -->

    <!-- Main JS-->
   <!--  <script src="js/global.js"></script> -->

    <script src="<?php echo base_url('/assets/evan/signup/vendor/jquery/jquery.min.js'); ?>"></script>
    <script src="<?php echo base_url('/assets/evan/signup/vendor/select2/select2.min.js'); ?>"></script>
    <script src="<?php echo base_url('/assets/evan/signup/vendor/datepicker/moment.min.js'); ?>"></script>
    <script src="<?php echo base_url('/assets/evan/signup/vendor/datepicker/daterangepicker.js'); ?>"></script>
    <script src="<?php echo base_url('/assets/evan/signup/vendor/js/global.js'); ?>"></script>

</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
<!-- end document-->