<!DOCTYPE html>
<html>
<head>
<title>Skynine Coffee</title>
<!-- <link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- <script src="assets/js/jquery-1.11.0.min.js"></script> -->
<!-- Custom Theme files -->
<!--theme-style-->
<!-- <link href="assets/css/style.css" rel="stylesheet" type="text/css" media="all" />	 --> 
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Free Style Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--fonts-->
<link href='http://fonts.googleapis.com/css?family=Alegreya+Sans+SC:100,300,400,500,700,800,900,100italic,300italic,400italic,500italic,700italic,800italic,900italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
<!--//fonts-->
<!-- <script type="text/javascript" src="assets/js/move-top.js"></script>
<script type="text/javascript" src="assets/js/easing.js"></script> -->
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>	
<!-- start menu -->
<!-- <script src="assets/js/simpleCart.min.js"> </script>
<link href="assets/css/memenu.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="assets/js/memenu.js"></script> -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/evan/css/bootstrap.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/evan/css/flexslider.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/evan/css/memenu.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/evan/css/style.css'); ?>">

<script src="<?php echo base_url('/assets/evan/js/easing.js'); ?>"></script>
<script src="<?php echo base_url('/assets/evan/js/jquery.flexslider.js'); ?>"></script>
<script src="<?php echo base_url('/assets/evan/js/jquery-1.11.0.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/evan/js/memenu.js'); ?>"></script>
<script src="<?php echo base_url('/assets/evan/js/move-top.js'); ?>"></script>
<script src="<?php echo base_url('/assets/evan/js/responsiveslides.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/evan/js/simpleCart.min.js'); ?>"></script>

<!-- <?php
	echo $css;
	echo $js;
?> -->

<script>$(document).ready(function(){$(".memenu").memenu();});</script>	
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5beaea5c70ff5a5a3a71fc00/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->			
</head>
<body> 
	<!--top-header-->
	<div class="top-header">
	<div class="container">
		<div class="top-header-main">
			<div class="col-md-4 top-header-left">
				<div class="search-bar">
					<input type="text" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}">
					<input type="submit" value="">
				</div>
			</div>
			<div class="col-md-4 top-header-middle">
				<!-- <a href="index.html"><img src="assets/images/logo-4.png" alt="" /></a> -->
				<a href="<?php echo base_url();?>index.php/NoLogin_HomeView_Controller"><img src="<?php echo base_url() ?>assets/evan/images/logo-skynine.png" alt="" /></a>
			</div>
			<div class="col-md-4 top-header-right">
				<div class="cart box_1">
					<!-- <img src="<?php echo base_url() ?>assets/evan/images/account.png" alt="" style="width: 40px; height: 19px;"/> -->
					<a href="<?php echo base_url();?>index.php/Login_Controller"><p style="color: black; font-size: 110%;">Login</p></a>
					<a href="<?php echo base_url();?>index.php/SignUp_Controller"><p style="color: black; font-size: 110%;">Sign up</p></a>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!--top-header-->
<!--bottom-header-->
	<div class="header-bottom">
		<div class="container">
			<div class="top-nav">
				<ul class="memenu skyblue"><li class="active"><a href="<?php echo base_url();?>index.php/NoLogin_HomeView_Controller">Home</a></li>
					<li class="grid"><a href="#">Shop</a>
						<div class="mepanel">
							<div class="row">
								<div class="col1 me-one">
									<ul>
										<a href="<?php echo base_url('/index.php/NoLogin_CoffeeList_Controller/index'); ?>"><img src="<?php echo base_url() ?>assets/evan/images/icon-coffee.png" alt="" style="width: 24px; height: 24px;" /></a>
									</ul>	
								</div>
								<div class="col1 me-one">
									<ul>
										<li><a href="<?php echo base_url('/index.php/NoLogin_CoffeeList_Controller/index'); ?>">Coffee</a></li>
									</ul>
								</div>
							</div>
							<div class="row">
								<div class="col1 me-one">
									<ul>
										<a href="<?php echo base_url('/index.php/NoLogin_CoffeeEq_Controller/index'); ?>"><img src="<?php echo base_url() ?>assets/evan/images/coffee-eq.png" alt="" style="width: 24px; height: 24px;" /></a>
									</ul>	
								</div>
								<div class="col1 me-one">
									<ul>
										<li><a href="<?php echo base_url('/index.php/NoLogin_CoffeeEq_Controller/index'); ?>">Equipment</a></li>
									</ul>	
								</div>
							</div>
						</div>
					</li>
					<li class="grid"><a href="<?php echo base_url('/index.php/NoLogin_CoffeeSubs_Controller/index'); ?>">Coffee Subscription</a>
					</li>
					</li>
					<li class="grid"><a href="<?php echo base_url();?>index.php/NoLogin_AboutUs_Controller">About Us</a>
					</li>
					<li class="grid"><a href="<?php echo base_url();?>index.php/NoLogin_Artikel_Controller">Articles</a>
					</li>
					<li class="grid"><a href="<?php echo base_url();?>index.php/NoLogin_Workshop_Controller">Workshop</a>
					</li>
					<li class="grid"><a href="<?php echo base_url();?>index.php/NoLogin_EventList_Controller">Event List</a></li>
					
				</ul>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
	<!--bottom-header-->
	<!--start-product--> 
	<div class="container">
		<h1 style="text-align: center; padding: 50px;">Kopi sebagai pembangkit semangat kerja?</h1>
		<img class="img-responsive col-md-4" src="<?php echo base_url() ?>assets/artikel/Artikel1.jpeg" alt="#" style="padding-left: 50px; width: 1100px; height: 500px; padding-right: 40px;"> 
	</div>
	<br>
	<br>
	<div class="container" style="padding-left: 50px; padding-right: 50px; text-align: justify; padding-top: 25px;">
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse lobortis magna eget enim rhoncus, sit amet maximus mi porta. Vivamus dictum hendrerit ipsum quis elementum. Curabitur aliquet magna sed augue placerat luctus. Nullam sollicitudin et nisl sed porta. Pellentesque efficitur aliquam nisi elementum aliquet. Nam aliquet, dui at sollicitudin feugiat, urna erat faucibus dui, in consectetur dolor dolor sit amet dolor. Curabitur augue ligula, dictum in semper in, ultricies id enim.</p>

		<p>Quisque et tellus a augue pellentesque hendrerit et a ante. Aenean non purus nec tortor suscipit faucibus. Cras elit nibh, egestas ac mollis et, sollicitudin at augue. Nunc at viverra turpis, eu consequat augue. Donec ut ornare diam, sit amet porta libero. Nam et ligula eget justo sodales vestibulum. Fusce nec mauris et ante faucibus suscipit. Maecenas et tellus ut leo volutpat placerat vitae eget dui. Aliquam a neque vitae ipsum congue rhoncus eget vitae arcu.</p>

		<p>In non elementum libero. Mauris fermentum consectetur orci, id vehicula ipsum vulputate id. Curabitur vitae cursus metus. Aenean eleifend, leo id tincidunt blandit, velit nunc luctus mi, ac eleifend nunc arcu vel augue. Curabitur sed magna eget felis hendrerit imperdiet. Phasellus ultrices, metus fringilla cursus tristique, ex ipsum dapibus sapien, in molestie libero turpis vel quam. Cras mollis enim in erat aliquet, at varius lectus tincidunt. Phasellus justo sapien, commodo nec fermentum nec, laoreet id dolor. Donec dictum, purus sed imperdiet molestie, ex est feugiat diam, in ultricies massa odio ac leo. Nulla commodo nisl sit amet auctor fermentum. Pellentesque congue interdum lorem. Donec aliquam lorem et maximus mattis.</p>

		<p>Phasellus sagittis urna ut purus dapibus, in mattis arcu venenatis. Sed id vulputate quam. Vestibulum pulvinar sem diam, non luctus arcu imperdiet quis. Ut vitae egestas mi. Morbi vitae felis eleifend, rutrum est sed, elementum sapien. Phasellus nec nisi id quam aliquam gravida. Duis vel blandit nisl, nec fermentum nulla. Vivamus metus sem, dignissim in orci sed, rutrum condimentum ipsum. Mauris turpis arcu, luctus et est at, pretium consequat lacus.</p>

		<p>Suspendisse tincidunt sem eget mollis placerat. Integer fringilla ut sapien ac molestie. Integer a tortor iaculis dolor egestas vestibulum. Etiam at erat vel tellus condimentum porta. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Suspendisse convallis nisi non nunc maximus fringilla. Nunc rhoncus volutpat tortor commodo lacinia. Curabitur molestie, lectus ac consequat eleifend, purus nisi rhoncus arcu, sed lacinia velit nulla ac nisl. Donec ornare dolor nec varius finibus. Suspendisse rhoncus diam ipsum. Nullam nec nibh bibendum quam tempor feugiat et eu sem.</p>
	</div>
	<!--end-product-->
	<!--start-footer-->
	<div class="footer">
		<div class="container">
			<div class="footer-top">
				<div class="col-md-3">
					<img src="<?php echo base_url() ?>assets/evan/images/logo-skynine.png" alt="" style="width: 90px; height: 90px;" />
					<br>
					<br>
					<h4>Skynine Coffee</h4>
					<ul>
						<div style="width: 200px; height: 31px;">
							<img src="<?php echo base_url() ?>assets/evan/images/contact-phone.png" alt="" style="width: 15px; height: 15px;" />
							<h10 style="padding-left: 1em; font-family: 'Lato', sans-serif; font-size: 14px;">089989898989</h10>
						</div>
						<div style="width:450px; height: 31px;">
							<img src="<?php echo base_url() ?>assets/evan/images/contact-address.png" alt="" style="width: 15px; height: 15px;" />
							<h10 style="padding-left: 1em; font-family: 'Lato', sans-serif; font-size: 14px;">Jl. Pelepah Kuning 2 No. 21, Gading Serpong, Tangerang</h10>
						</div>
						<div style="width:300px; height: 31px;">
							<img src="<?php echo base_url() ?>assets/evan/images/contact-email.png" alt="" style="width: 15px; height: 15px;" />
							<h10 style="padding-left: 1em; font-family: 'Lato', sans-serif; font-size: 14px;">skynine@gmail.com</h10>
						</div>
					</ul>
				</div>
				<div class="col-md-3 footer-left" style="float: right; padding-top: 100px;">
					<h5>Social Media</h5>
					<ul>
						<div style="width: 200px; height: 31px;">
							<img src="<?php echo base_url() ?>assets/evan/images/contact-fb.png" alt="" style="width: 40px; height: 30px; padding-right: 1em;" />
							<img src="<?php echo base_url() ?>assets/evan/images/contact-ig.png" alt="" style="width: 40px; height: 40px;" />
							<img src="<?php echo base_url() ?>assets/evan/images/contact-twit.png" alt="" style="width: 40px; height: 40px;" />
						</div>
					</ul>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!--end-footer-->
	<!--end-footer-text-->
	<div class="footer-text">
		<div class="container">
			<div class="footer-main">
				<p class="footer-class">Skynine Coffee<a href="http://w3layouts.com/" target="_blank"></a> </p>
			</div>
		</div>
		<script type="text/javascript">
			$(document).ready(function() {
				/*
				var defaults = {
		  			containerID: 'toTop', // fading element id
					containerHoverID: 'toTopHover', // fading element hover id
					scrollSpeed: 1200,
					easingType: 'linear' 
		 		};
				*/
				
				$().UItoTop({ easingType: 'easeOutQuart' });
				
			});
		</script>
		<a href="#home" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
	</div>
	<!--end-footer-text-->	

<!-- <script src="<?php echo base_url('/assets/js/easing.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/jquery.flexslider.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/jquery-1.11.0.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/memenu.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/move-top.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/responsiveslides.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/simpleCart.min.js'); ?>"></script> -->

</body>
</html>