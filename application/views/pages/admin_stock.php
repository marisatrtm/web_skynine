<!DOCTYPE html>
<html>
<head>
<title>Skynine Coffee</title>
<link href="<?php echo base_url('assets/ica/css/bootstrap.css'); ?>" rel="stylesheet" type="text/css" media="all" />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-1.11.0.min.js"></script>
<!-- Custom Theme files -->
<!--theme-style-->
<link href="<?php echo base_url('assets/ica/css/style.css'); ?>" rel="stylesheet" type="text/css" media="all" />	
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Free Style Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--fonts-->
<link href='http://fonts.googleapis.com/css?family=Alegreya+Sans+SC:100,300,400,500,700,800,900,100italic,300italic,400italic,500italic,700italic,800italic,900italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
<!--//fonts-->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>	
<!-- start menu -->
<script src="js/simpleCart.min.js"> </script>
<link href="css/memenu.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="js/memenu.js"></script>
<script>$(document).ready(function(){$(".memenu").memenu();});</script>				
</head>
<body> 
	<!--top-header-->
	<div class="top-header">
	<div class="container">
		<div class="top-header-main">
			<div class="col-md-4 top-header-left">
				<!-- <div class="search-bar">
					<input type="text" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}">
					<input type="submit" value="">
				</div> -->
			</div>
			<div class="col-md-4 top-header-middle">
				<a href="index.html"><img src="images/logo-4.png" alt="" /></a>
			</div>
			<div class="col-md-4 top-header-right">
				<!-- <div class="cart box_1">
					<a href="checkout.html">
					<h3> <div class="total">
						<span class="simpleCart_total"></span> (<span id="simpleCart_quantity" class="simpleCart_quantity"></span> items)</div>
						<img src="images/cart-1.png" alt="" />
					</a>
					<p><a href="javascript:;" class="simpleCart_empty">Empty Cart</a></p>
					<div class="clearfix"> </div>
				</div> -->
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!--top-header-->
	<!-- header bottom -->
	<div class="header-bottom">
		<div class="container">
			<div class="top-nav">
				<h2 style="text-align: center;">New Products</h2>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
	<!-- close header bottom -->

	<div>
		<!-- form -->
		<form class="form-horizontal">
		  <div class="form-group">
		    <label class="control-label col-sm-2" for="id">Product ID:</label>
		    <div class="col-sm-8">
		    	 <input type="id" class="form-control" id="product_id">
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="control-label col-sm-2" for="name">Product Name:</label>
		    <div class="col-sm-8">
		    	<input type="name" class="form-control" id="name">
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="control-label col-sm-2" for="details">Details:</label>
		    <div class="col-sm-8">
		    	<input type="name" class="form-control" id="name">
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="control-label col-sm-2" for="type">Product Type:</label>
		    <div class="col-sm-8">
		    	 <input type="type" class="form-control" id="type">
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="control-label col-sm-2" for="price">Price:</label>
		    <div class="col-sm-8">
		    	<input type="price" class="form-control" id="price">
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="control-label col-sm-2" for="addedby">Added By:</label>
		    <div class="col-sm-8">
		    	 <input type="addedby" class="form-control" id="addedby">
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="control-label col-sm-2" for="uploaded_date">Uploaded Date:</label>
		    <div class="col-sm-8">
		    	<input type="uploaded_date" class="form-control" id="uploaded_date">
		    </div>
		  </div>

		  <div class="col-sm-offset-2 col-sm-10">
		  		<a href="admin.html" type="cancel" class="btn btn-default">Cancel</a>
		 	 	<button type="submit" class="btn btn-primary">Add New Product</button>
		  </div>
		</form>
		
	</div>
	<!--end-abt-shoe-->
	<!--start-footer-->
	<div class="footer">
		<div class="container">
			<div class="footer-top">
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!--end-footer-->
	<!--end-footer-text-->
	<div class="footer-text">
		<div class="container">
			<div class="footer-main">
				<p class="footer-class">© 2015 Free Style All Rights Reserved | Design by  <a href="http://w3layouts.com/" target="_blank">W3layouts</a> </p>
			</div>
		</div>
		<script type="text/javascript">
			$(document).ready(function() {
				/*
				var defaults = {
		  			containerID: 'toTop', // fading element id
					containerHoverID: 'toTopHover', // fading element hover id
					scrollSpeed: 1200,
					easingType: 'linear' 
		 		};
				*/
				
				$().UItoTop({ easingType: 'easeOutQuart' });
				
			});
		</script>
		<a href="#home" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
	</div>
	<!--end-footer-text-->	
</body>
</html>