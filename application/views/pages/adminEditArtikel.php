<!DOCTYPE html>
<html>
<head>
<title>Skynine Coffee</title>
<link href="<?php echo base_url();?>assets/ica/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="<?php echo base_url();?>assets/ica/js/jquery-1.11.0.min.js"></script>
<!-- Custom Theme files -->
<!--theme-style-->
<link href="<?php echo base_url();?>assets/ica/css/style.css" rel="stylesheet" type="text/css" media="all" />	
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Free Style Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--fonts-->
<link href='http://fonts.googleapis.com/css?family=Alegreya+Sans+SC:100,300,400,500,700,800,900,100italic,300italic,400italic,500italic,700italic,800italic,900italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
<!--//fonts-->
<script type="text/javascript" src="<?php echo base_url();?>assets/ica/js/move-top.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/ica/js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>	
<!-- start menu -->
<script src="<?php echo base_url();?>assets/ica/js/simpleCart.min.js"> </script>
<link href="<?php echo base_url();?>assets/ica/css/memenu.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="<?php echo base_url();?>assets/ica/js/memenu.js"></script>
<script>$(document).ready(function(){$(".memenu").memenu();});</script>				
</head>
<body> 
	<!--top-header-->
	<div class="top-header">
	<div class="container">
		<div class="top-header-main">
			<div class="col-md-4 top-header-left">
				<!-- <div class="search-bar">
					<input type="text" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}">
					<input type="submit" value="">
				</div> -->
			</div>
			<div class="col-md-4 top-header-middle">
				<a href="<?php echo base_url()?>index.php/AddStock"><img src="<?php echo base_url()?>assets/images/skynine.jpg" alt="" style="width: 125px; height: 125px;" /></a>
			</div>
			<div class="col-md-4 top-header-right">
				<div class="cart box_1">
					<a href="<?php echo base_url('/index.php/Admin/logout'); ?>">LOGOUT
						<span class="glyphicon glyphicon-user"></span>
					</a>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!--top-header-->
	<!-- header bottom -->
	<div class="header-bottom">
		<div class="container">
			<div class="top-nav">
				<h2 style="text-align: center;">Edit Artickel</h2>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
	<!-- close header bottom -->

	<div>
		<!-- form -->
		<form class="form-horizontal">
		  <div class="form-group">
		    <label class="control-label col-sm-2" for="id">Artikel ID:</label>
		    <div class="col-sm-8">
		    	 <input type="id" class="form-control" id="artikel_id">
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="control-label col-sm-2" for="name">Judul Artikel:</label>
		    <div class="col-sm-8">
		    	<input type="name" class="form-control" id="judul">
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="control-label col-sm-2" for="details">Penulis:</label>
		    <div class="col-sm-8">
		    	<input type="name" class="form-control" id="author">
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="control-label col-sm-2" for="type">Konten:</label>
		    <div class="col-sm-8">
		    	 <textarea type="type" class="form-control" id="konten"></textarea>
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="control-label col-sm-2" for="price">Kategori:</label>
		    <div class="col-sm-8">
		    	<input type="price" class="form-control" id="kategori">
		    </div>
		  </div>
		   <div class="form-group">
		    <label class="control-label col-sm-2" for="uploaded_date">Added Date:</label>
		    <div class="col-sm-8">
		    	<input type="uploaded_date" class="form-control" id="added_date">
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="control-label col-sm-2" for="uploaded_date">Modified Date:</label>
		    <div class="col-sm-8">
		    	<input type="uploaded_date" class="form-control" id="modified_date">
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="control-label col-sm-2" for="uploaded_date">Publish:</label>
		    <div class="col-sm-8">
		    	<input type="uploaded_date" class="form-control" id="added_date">
		    </div> 
		  </div>
		  <div class="form-group">
		    <label class="control-label col-sm-2" for="addedby">Image:</label>
		    <form class="md-form">
			    <div class="file-field">
			        <div class="btn btn-rounded purple-gradient btn-sm float-left">
			            <input type="file">
			        </div>
			    </div>
			</form>
		  </div>
		 

		  <div class="col-sm-offset-2 col-sm-10">
		  		<a href="<?php echo base_url();?>index.php/Admin_Artikel_Controller" type="cancel" class="btn btn-default">Cancel</a>
		 	 	<a href="<?php echo base_url();?>index.php/Admin_Artikel_Controller"><button type="submit" class="btn btn-primary">Update Artikel</button></a>
		  </div>
		</form>
		
	</div>
	<!--end-abt-shoe-->
	<!--start-footer-->
	<div class="footer">
		<div class="container">
			<div class="footer-top">
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!--end-footer-->
	<!--end-footer-text-->
	<div class="footer-text">
		<div class="container">
			<div class="footer-main">
				<p class="footer-class"> Skynine Coffee <a href="#" target="_blank"></a> </p>
			</div>
		</div>
		<script type="text/javascript">
			$(document).ready(function() {
				/*
				var defaults = {
		  			containerID: 'toTop', // fading element id
					containerHoverID: 'toTopHover', // fading element hover id
					scrollSpeed: 1200,
					easingType: 'linear' 
		 		};
				*/
				
				$().UItoTop({ easingType: 'easeOutQuart' });
				
			});
		</script>
		<a href="#home" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
	</div>
	<!--end-footer-text-->	
</body>
</html>
