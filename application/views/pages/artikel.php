<!-- <!DOCTYPE html> -->
<html>
<head>
<title>Skynine Coffee</title>
<!-- <link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- <script src="assets/js/jquery-1.11.0.min.js"></script> -->
<!-- Custom Theme files -->
<!--theme-style-->
<!-- <link href="assets/css/style.css" rel="stylesheet" type="text/css" media="all" />	 --> 
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Free Style Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--fonts-->
<link href='http://fonts.googleapis.com/css?family=Alegreya+Sans+SC:100,300,400,500,700,800,900,100italic,300italic,400italic,500italic,700italic,800italic,900italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
<!--//fonts-->
<!-- <script type="text/javascript" src="assets/js/move-top.js"></script>
<script type="text/javascript" src="assets/js/easing.js"></script> -->
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>	
<!-- start menu -->
<!-- <script src="assets/js/simpleCart.min.js"> </script>
<link href="assets/css/memenu.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="assets/js/memenu.js"></script> -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/evan/css/bootstrap.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/evan/css/flexslider.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/evan/css/memenu.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/evan/css/style.css'); ?>">

<script src="<?php echo base_url('/assets/evan/js/easing.js'); ?>"></script>
<script src="<?php echo base_url('/assets/evan/js/jquery.flexslider.js'); ?>"></script>
<script src="<?php echo base_url('/assets/evan/js/jquery-1.11.0.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/evan/js/memenu.js'); ?>"></script>
<script src="<?php echo base_url('/assets/evan/js/move-top.js'); ?>"></script>
<script src="<?php echo base_url('/assets/evan/js/responsiveslides.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/evan/js/simpleCart.min.js'); ?>"></script>

<!-- <?php
	echo $css;
	echo $js;
?> -->

<script>$(document).ready(function(){$(".memenu").memenu();});</script>	

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5beaea5c70ff5a5a3a71fc00/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
			
</head>
<body> 
	<!--top-header-->
	<div class="top-header">
	<div class="container">
		<div class="top-header-main">
			<div class="col-md-4 top-header-left">
				<div class="search-bar">
					<input type="text" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}">
					<input type="submit" value="">
				</div>
			</div>
			<div class="col-md-4 top-header-middle">
				<a href="<?php echo base_url();?>index.php/Index_Controller"><img src="<?php echo base_url() ?>assets/evan/images/logo-skynine.png" alt="" /></a>
			</div>
			<div class="col-md-4 top-header-right">
				<div class="cart box_1">
					<!-- <img src="<?php echo base_url() ?>assets/evan/images/account.png" alt="" style="width: 40px; height: 19px;"/> -->
					<a href="<?php echo base_url();?>index.php/EditProfile_Controller"><p style="color: black; font-size: 110%;">Edit Profile</p></a>
					<a href="<?php echo base_url();?>index.php/TransactionHistory_Controller"><p style="color: black; font-size: 110%;">Transaction History</p></a>
					<a href="<?php echo base_url();?>index.php/Index_Controller/logout"><p style="color: black; font-size: 110%;">Log Out</p></a>
				</div>
				<div class="cart box_1">
					<a href="<?php echo base_url();?>index.php/AddToCart_Controller">
					<h3> <div class="total">
						<!-- <span class="simpleCart_total"></span> (<span id="simpleCart_quantity" class="simpleCart_quantity"></span> items)</div> -->
						<img src="<?php echo base_url() ?>assets/evan/images/cart-1.png" alt="" />
					</a>
					<!-- <p><a href="javascript:;" class="simpleCart_empty">Empty Cart</a></p> -->
					<div class="clearfix"> </div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!--top-header-->
<!--bottom-header-->
	<div class="header-bottom">
		<div class="container">
			<div class="top-nav">
				<ul class="memenu skyblue"><li class="active"><a href="<?php echo base_url();?>index.php/Index_Controller">Home</a></li>
					<li class="grid"><a href="#">Shop</a>
						<div class="mepanel">
							<div class="row">
								<div class="col1 me-one">
									<ul>
										<a href="<?php echo base_url('/index.php/Coffeelist_Controller/index'); ?>"><img src="<?php echo base_url() ?>assets/evan/images/icon-coffee.png" alt="" style="width: 24px; height: 24px;" /></a>
									</ul>	
								</div>
								<div class="col1 me-one">
									<ul>
										<li><a href="<?php echo base_url('/index.php/Coffeelist_Controller/index'); ?>">Coffee</a></li>
									</ul>
								</div>
							</div>
							<div class="row">
								<div class="col1 me-one">
									<ul>
										<a href="<?php echo base_url('/index.php/CoffeeEq_Controller/index'); ?>"><img src="<?php echo base_url() ?>assets/evan/images/coffee-eq.png" alt="" style="width: 24px; height: 24px;" /></a>
									</ul>	
								</div>
								<div class="col1 me-one">
									<ul>
										<li><a href="<?php echo base_url('/index.php/CoffeeEq_Controller/index'); ?>">Equipment</a></li>
									</ul>	
								</div>
							</div>
						</div>
					</li>
					<li class="grid"><a href="<?php echo base_url('/index.php/CoffeeSubs_Controller/index'); ?>">Coffee Subscription</a>
					</li>
					<li class="grid"><a href="<?php echo base_url();?>index.php/About_Controller">About Us</a>
					</li>
					<li class="grid"><a href="<?php echo base_url();?>index.php/Artikel_Controller">Articles</a>
					</li>
					<li class="grid"><a href="<?php echo base_url();?>index.php/Workshop_Controller">Workshop</a>
					</li>
					<li class="grid"><a href="<?php echo base_url();?>index.php/EventList_Controller">Event List</a></li>
				</ul>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
	<!--bottom-header-->
	<!--start-product--> 
	<div class="product">
		<h1 align="center">Skynine Article</h1>
		<br>
		<br>
		<br>
		<div class="container">
			<div class="product-main">
				<div class="col-md-9 p-left">
				<div class="product-one">
				<div class="col-md-4 product-left single-left"> 
					<div class="p-one simpleCart_shelfItem">
						<a href="<?php echo base_url();?>index.php/Artikel_Detail_Controller">
								<img src="<?php echo base_url() ?>assets/artikel/artikel1.jpeg" alt="" style="height: 180px;"/>
								<div class="mask mask1">
									<span>Quick View</span>
								</div>
							</a>
						<h5>Kopi sebagai pembangkit semangat kerja?</h5>
						<p></p>
					</div>
				</div>
				<div class="col-md-4 product-left single-left"> 
					<div class="p-one simpleCart_shelfItem">
						<a href="<?php echo base_url();?>index.php/Artikel_Detail_Controller">
								<img src="<?php echo base_url() ?>assets/artikel/artikel2.jpg" alt="" style="height: 180px;"/>
								<div class="mask mask1">
									<span>Quick View</span>
								</div>
							</a>
						<h5>Bagaimana memilih biji kopi yang baik dan benar</h5>
						<p></p>
					</div>
				</div>
				<div class="col-md-4 product-left single-left"> 
					<div class="p-one simpleCart_shelfItem">
						<a href="<?php echo base_url();?>index.php/Artikel_Detail_Controller">
								<img src="<?php echo base_url() ?>assets/artikel/artikel3.jpg" alt="" style="height: 180px;"/>
								<div class="mask mask1">
									<span>Quick View</span>
								</div>
							</a>
						<h5>Cara membedakan kopi yang baik dengan yang tidak</h5>
						<p></p>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="product-one">
				
			<div class="clearfix"> </div>
			</div>

			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
	</div>
	<!--end-product-->
	<!--start-footer-->
	<div class="footer">
		<div class="container">
			<div class="footer-top">
				<div class="col-md-3">
					<img src="<?php echo base_url() ?>assets/evan/images/logo-skynine.png" alt="" style="width: 90px; height: 90px;" />
					<br>
					<br>
					<h4>Skynine Coffee</h4>
					<ul>
						<div style="width: 200px; height: 31px;">
							<img src="<?php echo base_url() ?>assets/evan/images/contact-phone.png" alt="" style="width: 15px; height: 15px;" />
							<h10 style="padding-left: 1em; font-family: 'Lato', sans-serif; font-size: 14px;">089989898989</h10>
						</div>
						<div style="width:450px; height: 31px;">
							<img src="<?php echo base_url() ?>assets/evan/images/contact-address.png" alt="" style="width: 15px; height: 15px;" />
							<h10 style="padding-left: 1em; font-family: 'Lato', sans-serif; font-size: 14px;">Jl. Pelepah Kuning 2 No. 21, Gading Serpong, Tangerang</h10>
						</div>
						<div style="width:300px; height: 31px;">
							<img src="<?php echo base_url() ?>assets/evan/images/contact-email.png" alt="" style="width: 15px; height: 15px;" />
							<h10 style="padding-left: 1em; font-family: 'Lato', sans-serif; font-size: 14px;">skynine@gmail.com</h10>
						</div>
					</ul>
				</div>
				<div class="col-md-3 footer-left" style="float: right; padding-top: 100px;">
					<h5>Social Media</h5>
					<ul>
						<div style="width: 200px; height: 31px;">
							<img src="<?php echo base_url() ?>assets/evan/images/contact-fb.png" alt="" style="width: 40px; height: 30px; padding-right: 1em;" />
							<img src="<?php echo base_url() ?>assets/evan/images/contact-ig.png" alt="" style="width: 40px; height: 40px;" />
							<img src="<?php echo base_url() ?>assets/evan/images/contact-twit.png" alt="" style="width: 40px; height: 40px;" />
						</div>
					</ul>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!--end-footer-->
	<!--end-footer-text-->
	<div class="footer-text">
		<div class="container">
			<div class="footer-main">
				<p class="footer-class">Skynine Coffee<a href="#" target="_blank"></a> </p>
			</div>
		</div>
		<script type="text/javascript">
			$(document).ready(function() {
				/*
				var defaults = {
		  			containerID: 'toTop', // fading element id
					containerHoverID: 'toTopHover', // fading element hover id
					scrollSpeed: 1200,
					easingType: 'linear' 
		 		};
				*/
				
				$().UItoTop({ easingType: 'easeOutQuart' });
				
			});
		</script>
		<a href="#home" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
	</div>
	<!--end-footer-text-->	

<!-- <script src="<?php echo base_url('/assets/js/easing.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/jquery.flexslider.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/jquery-1.11.0.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/memenu.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/move-top.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/responsiveslides.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/simpleCart.min.js'); ?>"></script> -->

</body>
</html>